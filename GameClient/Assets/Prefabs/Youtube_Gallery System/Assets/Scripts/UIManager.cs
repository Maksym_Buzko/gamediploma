﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public CardManager cardManager;
    public GameObject[] cardSlots;

    public Color humanColor, orcColor, undeadColor, nightElfColor, neutralColor;

    private void Start()
    {
        DisplayCards();
    }

    private void DisplayCards()
    {
        for(int i = 0; i < cardManager.cards.Count; i++)
        {
            //MARKER Assign the cardManager's cards information to the UI references
            cardSlots[i].transform.GetChild(6).transform.GetChild(0).GetComponent<Text>().text = cardManager.cards[i].cardName;
            cardSlots[i].transform.GetChild(2).transform.GetChild(0).GetComponent<Text>().text = cardManager.cards[i].cardDes;

            cardSlots[i].transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = cardManager.cards[i].cardBgSprite;
            cardSlots[i].transform.GetChild(0).transform.GetChild(1).GetComponent<Image>().sprite = cardManager.cards[i].cardSprite;

            cardSlots[i].transform.GetChild(4).transform.GetChild(0).GetComponent<Text>().text = cardManager.cards[i].cardAttack.ToString();
            cardSlots[i].transform.GetChild(5).transform.GetChild(0).GetComponent<Text>().text = cardManager.cards[i].cardHealth.ToString();


            switch (cardManager.cards[i].cardClass)
            {
                case CardClass.Human:
                    {
                        cardSlots[i].transform.GetChild(3).transform.GetChild(0).GetComponent<Text>().text = "Human";
                        cardSlots[i].GetComponent<Image>().color = humanColor;
                        break;
                    }
                case CardClass.Orc:
                    {
                        cardSlots[i].transform.GetChild(3).transform.GetChild(0).GetComponent<Text>().text = "Orc";
                        cardSlots[i].GetComponent<Image>().color = orcColor;
                        break;
                    }
                case CardClass.Undead:
                    {
                        cardSlots[i].transform.GetChild(3).transform.GetChild(0).GetComponent<Text>().text = "Undead";
                        cardSlots[i].GetComponent<Image>().color = undeadColor;
                        break;
                    }
                case CardClass.NightElf:
                    {
                        cardSlots[i].transform.GetChild(3).transform.GetChild(0).GetComponent<Text>().text = "NightElf";
                        cardSlots[i].GetComponent<Image>().color = nightElfColor;
                        break;
                    }
                case CardClass.Neutral:
                    {
                        cardSlots[i].transform.GetChild(3).transform.GetChild(0).GetComponent<Text>().text = "Neutral";
                        cardSlots[i].GetComponent<Image>().color = neutralColor;
                        break;
                    }
            }

        }
    }

}
