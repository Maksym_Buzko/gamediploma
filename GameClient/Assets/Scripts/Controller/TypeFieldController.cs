using System;
using System.Collections.Generic;
using GameClient.Model.Unit;
using UnityEngine;

namespace GameClient
{
    public class TypeFieldController : MonoBehaviour
    {
        public UnitType UnitType;
        public bool IsEnemyField;
        public int CardsCount;
        private float _step = 0.15f;
        private RectTransform _fieldPosition;
        [NonSerialized] public List<Vector3> CardPositions;

        // Start is called before the first frame update
        void Start()
        {
            _fieldPosition = gameObject.GetComponent<RectTransform>();
            CardPositions = new List<Vector3>();
            var counter = 0;

            for (var i = 0; i < 5; i++)
            {
                if (i % 2 == 0)
                {
                    var localPosition = _fieldPosition.localPosition;
                    CardPositions.Add(new Vector3(localPosition.x + _step * _fieldPosition.rect.width * counter, 0, 0));
                }
                else
                {
                    counter++;
                    var localPosition = _fieldPosition.localPosition;
                    CardPositions.Add(new Vector3(localPosition.x - _step * _fieldPosition.rect.width * counter, 0, 0));
                }
            }
        }

        public Vector3 GetNextPosition()
        {
            if (CardsCount < 5)
                return CardPositions[CardsCount++];

            throw new IndexOutOfRangeException("Can't place more than 5 cards to the field");
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
