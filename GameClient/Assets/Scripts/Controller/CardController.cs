using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameClient
{
    public class CardController : MonoBehaviour
    {
        public Image MainImage;
        public TextMeshProUGUI Health;
        public TextMeshProUGUI Attack;
        public UnitCard CardInfo;

        public Vector3 StartingPosition;
        public Transform Parent;

        // Start is called before the first frame update
        void Start()
        {
            // MainImage.sprite = Resources.Load<Sprite>("Warriors/cavelry2");

            var currTransform = transform;
            StartingPosition = currTransform.position;
            Parent = currTransform.parent;
        }

        public void UpdateCardData()
        {
            MainImage.sprite = Resources.Load<Sprite>($"Warriors/{CardInfo.Name}");
            Health.text = $"{CardInfo.Defense}";
            Attack.text = $"{CardInfo.Attack}";
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
