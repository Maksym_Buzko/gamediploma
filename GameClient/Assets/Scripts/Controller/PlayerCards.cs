using System.Collections.Generic;
using System.Linq;

namespace GameClient.Controller
{
  public static class PlayerCards
  {
    public static List<UnitCard> OldCards = new List<UnitCard>();
    public static List<UnitCard> CurrentCards = new List<UnitCard>();

    public static List<UnitCard> RemainedOldCards = new List<UnitCard>();
    public static List<UnitCard> RemainedCards = new List<UnitCard>();

    public static void Initialize(List<UnitCard> current, List<UnitCard> remaining)
    {
      OldCards = current;
      CurrentCards = current.ToList();
      RemainedOldCards = remaining;
      RemainedCards = remaining.ToList();
    }

    public static void RollbackCards()
    {
      CurrentCards = OldCards.ToList();
      RemainedCards = RemainedOldCards.ToList();
    }

    public static void ApplyChanges()
    {
      OldCards = CurrentCards.ToList();
      RemainedOldCards = RemainedCards.ToList();
    }
  }
}
