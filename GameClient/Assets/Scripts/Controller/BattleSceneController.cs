using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using GameClient.Controller;
using GameClient.Model;
using GameClient.Model.Unit;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace GameClient
{
  public class BattleSceneController : MonoBehaviour
  {
    public Button PassButton;
    public Button ExitButton;
    public TextMeshProUGUI MyPower;
    public TextMeshProUGUI EnemyPower;
    public TextMeshProUGUI MyRoundScore;
    public TextMeshProUGUI EnemyRoundScore;
    public List<CardController> MyCards;
    public int Id;
    public List<Image> EnemyCards;
    public List<TypeFieldController> EnemyTypeFields;
    public bool IsPlayersTurn => _currentGame?.PLayersTurn == Id;

    public CardController CardPrefab;
    public GameObject WinScreen;
    public GameObject LooseScreen;
    public GameObject RoundFinished;
    public GameObject EnemyPassed;
    public GameObject YouPassed;


    private Game innerGameData;
    private event Action _gameDataValueChanged;
    private Game _currentGame
    {
      get => innerGameData;
      set
      {
        innerGameData = value;
        _gameDataValueChanged?.Invoke();
      }
    }

    private StaticData _staticData;
    private List<int> _otherPlayerBoard = new List<int>();
    private List<CardController> _cardsOnBoard = new List<CardController>();
    private int _lastRoundNum = 0;
    private bool _winnerDisplayed;
    private (bool, int) _enemyPassedInRound;

    public enum HttpMethod
    {
      // ReSharper disable InconsistentNaming
      GET,
      POST,
    }

    public void TurnDone(CardController card)
    {
      SendRequest<Game>(HttpMethod.POST,
        MakeUrl($"GameProcess/placecard?gameId={_currentGame.Id}&playerId={Id}&cardId={card.CardInfo.Id}"),
        game => _currentGame = game, null);
    }

    // Start is called before the first frame update
    void Start()
    {
      _gameDataValueChanged += OnGameDataChanged;
      StartCoroutine(Autorefresh());
      SendRequest<StaticData>(HttpMethod.GET, MakeUrl("StaticData"), sd =>
      {
        _staticData = sd;
        SetCards();
      }, null);

      PassButton.onClick.AddListener(() => SendRequest<Game>(HttpMethod.POST,
        MakeUrl($"GameProcess/pass?gameId={_currentGame.Id}&playerId={Id}"), game =>
        {
          _currentGame = game;
          YouPassed.SetActive(true);
          StartCoroutine(InvertActiveStateAfterSeconds(YouPassed, 2));
        }, null));
      ExitButton.onClick.AddListener(() => SendRequest<Game>(HttpMethod.POST,
        MakeUrl($"GameProcess/giveUp?gameId={_currentGame.Id}&playerId={Id}"), game =>
        {
          _currentGame = game;
          GetComponent<ChangeSceneController>().ChangeScene("MainMenu");
        }, null));

      Thread.Sleep(3000);
    }

    private IEnumerator Autorefresh()
    {
      for (;;)
      {
        SendRequest<Game>(HttpMethod.GET, MakeUrl("GameProcess?id=1"), game =>
        {
          _currentGame = game;
        }, null);

        yield return new WaitForSeconds(3f);
        yield return new WaitUntil(() => _currentGame?.PLayersTurn != Id); // stops autoRefresh on your turn
      }
    }

    private void SetCards()
    {
      if (Id == _currentGame.FirstPlayerId)
      {
        var cardsData = _staticData.Cards.Where(x => _currentGame.FirstPlayerHand.Contains(x.Id)).ToList();
        var count = 0;
        foreach (var card in MyCards)
        {
          card.CardInfo = cardsData[count++];
          card.UpdateCardData();
        }
      }
      else
      {
        var cardsData = _staticData.Cards.Where(x => _currentGame.SecondPlayerHand.Contains(x.Id)).ToList();
        var count = 0;
        foreach (var card in MyCards)
        {
          card.CardInfo = cardsData[count++];
          card.UpdateCardData();
        }
      }
    }

    private void OnGameDataChanged()
    {
      ProcessWinner();
      ProcessSomeonePassed();
      ProcessRoundChanged();
      ProcessNewEnemyCardOnBoard();
      ProcessPowerAndScoreChangesOnUi();
    }

    private void ProcessWinner()
    {
      if (_currentGame.Winner == -1 || _winnerDisplayed)
        return;

      if (_currentGame.Winner == Id)
      {
        WinScreen.SetActive(true);
        StartCoroutine(InvertActiveStateAfterSeconds(WinScreen, 2));
      }
      else
      {
        LooseScreen.SetActive(true);
        StartCoroutine(InvertActiveStateAfterSeconds(LooseScreen, 2));
      }

      _winnerDisplayed = true;
    }

    private void ProcessSomeonePassed()
    {
      if (_enemyPassedInRound.Item1 && _currentGame.RoundNumber == _enemyPassedInRound.Item2)
        return;

      if (Id == _currentGame.FirstPlayerId && _currentGame.SecondPlayerPassed)
      {
        EnemyPassed.SetActive(true);
        StartCoroutine(InvertActiveStateAfterSeconds(EnemyPassed, 2));
        _enemyPassedInRound = (true, _currentGame.RoundNumber);
      }

      if (Id == _currentGame.SecondPlayerId && _currentGame.FirstPlayerPassed)
      {
        EnemyPassed.SetActive(true);
        StartCoroutine(InvertActiveStateAfterSeconds(EnemyPassed, 2));
      }
    }

    private void ProcessRoundChanged()
    {
      if (_currentGame.RoundNumber <= _lastRoundNum)
        return;

      foreach (var card in _cardsOnBoard)
        Destroy(card.gameObject);

      _cardsOnBoard.Clear();

      RoundFinished.SetActive(true);
      StartCoroutine(InvertActiveStateAfterSeconds(RoundFinished, 2));
      _lastRoundNum = _currentGame.RoundNumber;
    }

    private void ProcessNewEnemyCardOnBoard()
    {
      var newId = Id == _currentGame.FirstPlayerId
        ? _currentGame.SecondPlayerBoard.Except(_otherPlayerBoard)
        : _currentGame.FirstPlayerBoard.Except(_otherPlayerBoard);

      var cardId = newId.FirstOrDefault();
      if (cardId == 0)
        return;

      PlaceEnemyCardOntoBoard(cardId);

      _otherPlayerBoard = Id == _currentGame.FirstPlayerId
        ? _currentGame.SecondPlayerBoard
        : _currentGame.FirstPlayerBoard;
    }

    private void PlaceEnemyCardOntoBoard(int cardId)
    {
      var cardData = _staticData.Cards.First(x => cardId == x.Id);
      var typeFieldController = EnemyTypeFields.First(x => x.UnitType == cardData.Type);
      var newCard = Instantiate(CardPrefab, typeFieldController.transform);
      newCard.transform.localPosition = typeFieldController.GetNextPosition();
      newCard.CardInfo = _staticData.Cards.First(x => x.Id == cardId);
      newCard.UpdateCardData();

      AddToCardsOnBoard(newCard);

      var newCardGameObject = newCard.gameObject;
      newCardGameObject.SetActive(false);
      StartCoroutine(InvertActiveStateAfterSeconds(newCardGameObject, 1));

      var lastCard = EnemyCards.Last();
      EnemyCards.Remove(lastCard);

      StartCoroutine(MoveToPosition(lastCard.transform, newCard.transform.position, 1));
      Destroy(lastCard, 1.1f);
    }

    public void AddToCardsOnBoard(CardController newCard)
    {
      _cardsOnBoard.Add(newCard);
    }

    private void ProcessPowerAndScoreChangesOnUi()
    {
      MyPower.text = (Id == _currentGame.FirstPlayerId
        ? (int) _currentGame.FirstPlayerBoardPower
        : (int) _currentGame.SecondPlayerBoardPower).ToString();

      EnemyPower.text = (Id == _currentGame.FirstPlayerId
        ? (int) _currentGame.SecondPlayerBoardPower
        : (int) _currentGame.FirstPlayerBoardPower).ToString();

      MyRoundScore.text = (Id == _currentGame.FirstPlayerId
        ? _currentGame.FirstPlayerScore
        : _currentGame.SecondPlayerScore) + "/2";

      EnemyRoundScore.text = (Id == _currentGame.FirstPlayerId
        ? _currentGame.SecondPlayerScore
        : _currentGame.FirstPlayerScore) + "/2";
    }

    // Update is called once per frame
    void Update()
    {
    }

    #region [Server Communication]

    private static string MakeUrl(string methodWithParams) => $"{ServerData.ServerUrl}/{methodWithParams}";

    private void SendRequest<T>(HttpMethod method, string url, Action<T> callbackOnSuccess, Action<string> callbackOnFail)
    {
      StartCoroutine(RequestCoroutine(method, url, callbackOnSuccess, callbackOnFail));
    }

    private IEnumerator RequestCoroutine<T>(HttpMethod method, string url, Action<T> callbackOnSuccess, Action<string> callbackOnFail)
    {
      var www = method == HttpMethod.GET ?  UnityWebRequest.Get(url) : UnityWebRequest.Post(url, string.Empty);
      yield return www.SendWebRequest();
      if (www.isNetworkError || www.isHttpError)
      {
        Debug.LogError(www.error);
        callbackOnFail?.Invoke(www.error);
      }
      else
      {
        Debug.Log(www.downloadHandler.text);
        ParseResponse(www.downloadHandler.text, callbackOnSuccess, callbackOnFail);
      }
    }

    private void ParseResponse<T>(string data, Action<T> callbackOnSuccess, Action<string> callbackOnFail)
    {
      var parsedData = JsonConvert.DeserializeObject<T>(data);
      callbackOnSuccess?.Invoke(parsedData);
    }
    #endregion

    public IEnumerator MoveToPosition(Transform transform, Vector3 position, float timeToMove)
    {
      var currentPos = transform.position;
      var t = 0f;
      while(t < 1)
      {
        t += Time.deltaTime / timeToMove;
        transform.position = Vector3.Lerp(currentPos, position, t);
        yield return null;
      }
    }

    public IEnumerator InvertActiveStateAfterSeconds(GameObject gameObject, float seconds)
    {
      yield return new WaitForSeconds(seconds);
      gameObject.SetActive(!gameObject.activeSelf);
    }
  }
}
