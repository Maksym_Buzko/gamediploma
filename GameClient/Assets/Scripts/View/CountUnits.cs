using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace GameClient
{
    public class CountUnits : MonoBehaviour
    {
        public Transform CurrentContent;

        public TextMeshProUGUI Text;
        // Start is called before the first frame update
        void Start()
        {
            Text.text = $"Units: {CurrentContent.childCount}/25";
        }

        // Update is called once per frame
        void Update()
        {
            Text.text = $"Units: {CurrentContent.childCount}/25";
        }
    }
}
