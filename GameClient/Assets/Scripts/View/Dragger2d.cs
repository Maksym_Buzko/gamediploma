using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameClient
{
    public class Dragger2d : MonoBehaviour
    {
        public GameObject UICanvas;
        private BattleSceneController _battleController;
        private GraphicRaycaster _raycaster;
        private PointerEventData _clickData;

        private List<RaycastResult> _clickedResults;
        private List<GameObject> _clickedElements;

        private Vector3 _mousePos;
        private Vector3 _mousePosOld;
        private Vector3 _startingPosition;

        private bool _isDragging;
        private GameObject _draggedElem;

        public void Start()
        {
            _raycaster = UICanvas.GetComponent<GraphicRaycaster>();
            _clickData = new PointerEventData(EventSystem.current);
            _clickedElements = new List<GameObject>();
            _clickedResults = new List<RaycastResult>();
            _battleController = GetComponent<BattleSceneController>();
        }

        public void Update()
        {
            if(!_battleController.IsPlayersTurn)
                return;

            _mousePos = Input.mousePosition;

            if (Input.GetMouseButtonDown(0))
                GetUIClicked();
            else if (Input.GetMouseButton(0) && _isDragging)
                DragElement();
            else
            {
                if (_isDragging)
                    TryAttachCardToField();
                _isDragging = false;
                _draggedElem = null;
            }

            _mousePosOld = _mousePos;
        }

        public void GetUIClicked()
        {
            _clickData.position = _mousePos;
            _clickedElements.Clear();
            _clickedResults.Clear();
            _raycaster.Raycast(_clickData, _clickedResults);
            _clickedElements = _clickedResults.Select(x => x.gameObject).ToList();

            var card = FindByTag(_clickedElements, "BattleCard");
            if (card == null)
                return;

            _isDragging = true;
            _draggedElem = card;
            _startingPosition = _mousePos;
        }

        public void DragElement()
        {
            var rectTransform = _draggedElem.GetComponent<RectTransform>();
            Vector2 mouseVector = _mousePos - _mousePosOld;

            rectTransform.anchoredPosition += mouseVector;
        }

        private void TryAttachCardToField()
        {
            var card = _draggedElem;
            var cardController = card.GetComponent<CardController>();
            var cardType = cardController.CardInfo?.Type;

            _clickData.position = _mousePos;
            _raycaster.Raycast(_clickData, _clickedResults);
            _clickedElements = _clickedResults.Select(x => x.gameObject).ToList();
            var field = FindByTag(_clickedElements, "Field");
            if (field == null)
            {
                card.transform.position = cardController.StartingPosition;
                return;
            }

            var fieldController = field.GetComponent<TypeFieldController>();
            if (fieldController == null || fieldController.UnitType != cardType)
            {
                card.transform.position = cardController.StartingPosition;
                return;
            }

            card.transform.SetParent(field.transform);
            card.transform.localPosition = fieldController.GetNextPosition();

            _battleController.AddToCardsOnBoard(cardController);
            _battleController.TurnDone(cardController);
        }

        private GameObject FindByTag(List<GameObject> elements, string tagToFind)
        {
            for (var i = 0; i < elements.Count; i++)
            {
                if (elements[i].CompareTag(tagToFind))
                    return elements[i];
            }

            return null;
        }
    }
}
