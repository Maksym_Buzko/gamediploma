using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameClient
{
    public class SetActiveFalse : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            Deactivate();
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
