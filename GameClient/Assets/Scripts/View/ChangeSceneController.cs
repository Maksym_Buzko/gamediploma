using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameClient.Controller
{
  public class ChangeSceneController : MonoBehaviour
  {

    public void ChangeScene(string sceneName)
    {
      SceneManager.LoadScene (sceneName);
    }
    public void Exit()
    {
      Application.Quit ();
    }


  }
}
