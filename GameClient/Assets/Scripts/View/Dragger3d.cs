using UnityEngine;
using UnityEngine.EventSystems;

namespace GameClient
{
    public class Dragger3d : DraggableObject, IPointerDownHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler
    {
        private Vector3 _dragOffset;
        private Camera _camera;
        private float _zCoordinate;

        private void Awake()
        {
           _camera = Camera.main;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            var position = transform.position;
            _zCoordinate = _camera.WorldToScreenPoint(position).z;
            _dragOffset = position - GetCursorWorldPos(eventData);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {

        }

        public void OnDrag(PointerEventData eventData)
        {
            transform.position = GetCursorWorldPos(eventData) + _dragOffset;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Debug.Log("Ended");
        }

        private Vector3 GetCursorWorldPos(PointerEventData eventData)
        {
            var cursorPoint = new Vector3(eventData.position.x, eventData.position.y, 0)
            {
                z = _zCoordinate
            };

            return _camera.ScreenToWorldPoint(cursorPoint);
        }

        public void OnDrop(PointerEventData eventData)
        {
            Debug.Log($"Dropped {this}");
        }
    }
}
