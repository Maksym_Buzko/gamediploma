using UnityEngine;

namespace GameClient
{
    public abstract class DraggableObject : MonoBehaviour
    {
        public DraggableObject DragItem { get; protected set; }
    }
}