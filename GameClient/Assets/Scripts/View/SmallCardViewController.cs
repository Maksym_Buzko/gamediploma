using System.Linq;
using GameClient.Controller;
using UnityEngine;
using UnityEngine.UI;

namespace GameClient
{
    public class SmallCardViewController : MonoBehaviour
    {
        public int Id;
        public GameObject AddButton;
        public GameObject DeleteButton;
        public Transform CurrentContent;
        public Transform RemainingContent;

        // Start is called before the first frame update
        void Start()
        {
            SetButtonsState();
            AddButton.GetComponent<Button>().onClick.AddListener(() =>
            {
                ChangeParent();
                SetButtonsState();
            });
            DeleteButton.GetComponent<Button>().onClick.AddListener(() =>
            {
                ChangeParent();
                SetButtonsState();
            });
        }

        private void SetButtonsState()
        {
            var isCurrent = AddButton.activeSelf;
            AddButton.SetActive(!isCurrent);
            DeleteButton.SetActive(isCurrent);
        }

        private void ChangeParent()
        {
            var addButtonActive = AddButton.activeSelf;
            if (addButtonActive)
                transform.parent.transform.SetParent(CurrentContent);
            else
                transform.parent.transform.SetParent(RemainingContent);
        }

        private bool IsCurrent()
        {
            return PlayerCards.CurrentCards.FirstOrDefault(x => x.Id == Id) != null;
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
