using System.Collections.Generic;

namespace GameClient.Model
{
  public class StaticData
  {
    public List<UnitCard> Cards { get; set; }
  }
}
