using System.Collections.Generic;

namespace GameClient
{
  public class User
  {
    public int Id;
    public string NickName;
    public string Email;
    public List<int> CardIds;
    public List<int> AllowedCardIds;
  }
}
