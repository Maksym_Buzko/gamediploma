using System.Collections.Generic;

namespace GameClient.Model.Unit
{
  public class Game
  {
    public int Id { get; set; }

    public int FirstPlayerId { get; set; }
    public int SecondPlayerId { get; set; }

    public int FirstPlayerFraction { get; set; }
    public int SecondPlayerFraction { get; set; }

    public int PLayersTurn { get; set; }

    public List<int> FirstPlayerBoard { get; set; }
    public double FirstPlayerBoardPower { get; set; }
    public bool FirstPlayerPassed { get; set; }

    public List<int> SecondPlayerBoard { get; set; }
    public double SecondPlayerBoardPower { get; set; }
    public bool SecondPlayerPassed { get; set; }

    public List<int> FirstPlayerDeck { get; set; }
    public List<int> FirstPlayerHand { get; set; }
    public List<int> FirstPlayerUsedCards { get; set; }

    public List<int> SecondPlayerDeck { get; set; }
    public List<int> SecondPlayerHand { get; set; }
    public List<int> SecondPlayerUsedCards { get; set; }

    public int RoundNumber { get; set; }
    public int FirstPlayerScore { get; set; }
    public int SecondPlayerScore { get; set; }

    public int Winner { get; set; } = -1;
  }
}
