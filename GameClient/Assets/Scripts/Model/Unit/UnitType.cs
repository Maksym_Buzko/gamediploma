namespace GameClient.Model.Unit
{
  public enum UnitType
  {
    Footman,
    Marksman,
    Cavalry,
    Artillery
  }
}
