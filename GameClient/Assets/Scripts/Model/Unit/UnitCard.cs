using GameClient.Model.Unit;

namespace GameClient
{
  public class UnitCard
  {
    public int Id { get; set; }

    public int FractionId { get; set; }

    public string Name { get; set; }

    public UnitType Type { get; set; }

    public RarityType Rarity { get; set; }

    public int Attack { get; set; }

    public int Defense { get; set; }
  }
}
