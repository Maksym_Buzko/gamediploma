namespace GameClient.Model.Unit
{
  public enum RarityType
  {
    Common,
    Rare,
    Epic,
    Legendary
  }
}
