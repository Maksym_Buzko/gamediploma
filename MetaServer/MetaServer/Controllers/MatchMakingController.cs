﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MetaServer.MatchMaking;
using MetaServer.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MetaServer.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class MatchMakingController : Controller
    {
        private IUserService _userService;
        private IMatchMakingQueue _matchMakingQueue;

        public MatchMakingController(IUserService userService, IMatchMakingQueue matchMakingQueue)
        {
            _userService = userService;
            _matchMakingQueue = matchMakingQueue;
        }

        [HttpGet]
        public IActionResult FindGame([FromQuery] int id)
        {
            var user = _userService.GetById(id);

            if (user != null)
            {
                _matchMakingQueue.Players.AddOrUpdate(id, 500, ((key, oldValue) => 500));
                
                Thread.Sleep(3000);

                var match = _matchMakingQueue.Matches.FirstOrDefault(x => x.Value.Equals(id));

                if (match.Key != 0 && match.Value != 0)
                {
                    var battle = new Battle()
                    {
                        FirstPlayer = match.Key,
                        SecondPlayer = match.Value,
                        Id = Guid.NewGuid()
                    };
                    
                    //request to battle server and return link and guid
                }
                else
                {
                    int a;
                    _matchMakingQueue.Players.Remove(id, out a);
                    _matchMakingQueue.Matches.AddOrUpdate(id, 0, (i, i1) => 0);
                    
                    while (true)
                    {
                        var secondPlayer = _matchMakingQueue.Players.FirstOrDefault().Key;

                        if (secondPlayer != 0)
                        {
                            var battle = new Battle()
                            {
                                FirstPlayer = id,
                                SecondPlayer = secondPlayer,
                                Id = Guid.NewGuid()
                            };
                            
                            //request to battle server and return link and guid
                            
                            break;
                        }
                    }
                }
            }
            else
            {
                BadRequest();
            }
            
            return Ok();
        }
        
    }
}