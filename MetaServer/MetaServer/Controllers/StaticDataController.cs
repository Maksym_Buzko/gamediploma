﻿using MetaServer.StaticData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MetaServer.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class StaticDataController : Controller
    {
        private IStaticData _staticData;

        public StaticDataController(IStaticData staticData)
        {
            _staticData = staticData;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult getStaticData()
        {
            var staticData = _staticData;
            return Ok(staticData);
        }
    }
}