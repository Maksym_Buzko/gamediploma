﻿using System.Collections.Generic;
using MetaServer.Services;
using MetaServer.StaticData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MetaServer.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class DecksController : Controller
    {
        private IDeckService _deckService;

        public DecksController(IDeckService deckService)
        {
            _deckService = deckService;
        }
        
        [HttpGet("{id}")]
        public IActionResult GetUserDeck(int id)
        {
            var userDeck = _deckService.GetDeck(id);
            return Ok(userDeck);
        }
        
        [HttpPost("{id}")]
        public IActionResult EditUserDeck(int id, [FromBody] List<int> cardIds)
        {
            var userDeck = _deckService.EditDeck(id, cardIds);
            return Ok(userDeck);
        }
    }
}