﻿using System;
using System.Collections.Generic;
using System.Linq;
using MetaServer.StaticData;

namespace MetaServer.Services
{
    public interface IDeckService
    {
        public List<int> GetDeck(int id);

        public List<int> EditDeck(int id, List<int> cardIds);
    }

    public class DeckService : IDeckService
    {
        private DataContext _context;

        public DeckService(
            DataContext context
            )
        {
            _context = context;
        }

        public List<int> GetDeck(int id)
        {
            var userDeck = _context.Users.SingleOrDefault(x => x.Id == id).CardIds;
            return userDeck;
        }

        public List<int> EditDeck(int id, List<int> cardIds)
        {
            var user = _context.Users.SingleOrDefault(x => x.Id == id);

            if (ValidateDeck(id, cardIds))
            {
                user.CardIds = cardIds;
                _context.Users.Update(user);
                _context.SaveChanges();
                var userDeck = user.CardIds;
                return userDeck;
            }
            else
            {
                throw new ArgumentException("InvalidDeck");
            }
        }

        private bool ValidateDeck(int id, List<int> cardIds)
        {
            var userAllowedCards = _context.Users.SingleOrDefault(x => x.Id == id).AllowedCardIds;
            var result = userAllowedCards.All(cardIds.Contains);
            return result;
        }
    }
}