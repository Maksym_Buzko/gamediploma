﻿using System.ComponentModel.DataAnnotations;

namespace MetaServer.UserEntities
{
    public class AuthenticationModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}