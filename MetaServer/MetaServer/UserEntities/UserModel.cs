﻿using System.Collections.Generic;

namespace MetaServer.UserEntities
{
    public class UserModel
    {
        public int Id { get; set; }
        public string NickName { get; set; }
        public string Email { get; set; }
        public List<int> CardIds { get; set; }
    }
}