﻿namespace MetaServer.UserEntities
{
    public class UpdateModel
    {
        public string NickName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}