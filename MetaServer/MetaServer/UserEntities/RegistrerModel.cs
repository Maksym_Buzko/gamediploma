﻿using System.ComponentModel.DataAnnotations;

namespace MetaServer.UserEntities
{
    public class RegisterModel
    {
        [Required]
        public string NickName { get; set; }
        
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}