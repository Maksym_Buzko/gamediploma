﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace MetaServer.MatchMaking
{
    public interface IMatchMakingQueue
    {
        public ConcurrentDictionary<int, int> Players { get; set; }
        public ConcurrentDictionary<int, int> Matches { get; set; }
        public List<Battle> Battles { get; set; }
    }
    
    public class MatchMakingQueue : IMatchMakingQueue
    {
        public ConcurrentDictionary<int, int> Players { get; set; }
        public ConcurrentDictionary<int, int> Matches { get; set; }
        public List<Battle> Battles { get; set; }

        public MatchMakingQueue()
        {
            Players = new ConcurrentDictionary<int, int>();
            Matches = new ConcurrentDictionary<int, int>();
            Battles = new List<Battle>();
        }
    }

    public class Battle
    {
        public Guid Id { get; set; }
        public int FirstPlayer { get; set; }
        public int SecondPlayer { get; set; }
    }
}