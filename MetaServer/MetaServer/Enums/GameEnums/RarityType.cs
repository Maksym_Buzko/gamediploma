﻿namespace MetaServer.Enums.GameEnums
{
    public enum RarityType
    {
        Common,
        Rare,
        Epic,
        Legendary
    }
}