﻿namespace MetaServer.Enums.GameEnums
{
    public enum UnitType
    {
        Footman,
        Marksman,
        Cavalry,
        Artillery
    }
}