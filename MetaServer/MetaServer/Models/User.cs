﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MetaServer.Models
{
    public class User
    {
        [Required]
        public int Id { get; set; }
        
        [Required]
        public string NickName { get; set; }
        
        [Required]
        public string Email { get; set; }
        
        public byte[] PasswordHash { get; set; }
        
        public byte[] PasswordSalt { get; set; }
        
        public List<int> CardIds { get; set; }
        
        public List<int> AllowedCardIds { get; set; }
    }
}