﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;

namespace MetaServer.StaticData
{
    public static class StaticDataParser
    {
        public static List<T> ReadDataFromCSV<T>()
        {
            using (var reader = new StreamReader("cards.csv"))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                var records = csv.GetRecords<T>();
                return records.ToList();
            }
        }
    }
}