﻿using System.Collections.Generic;
using MetaServer.GameModels.Cards;

namespace MetaServer.StaticData
{
    public interface IStaticData
    {
        public List<UnitCard> Cards { get; }
    }

    public class StaticData : IStaticData
    {
        public List<UnitCard> Cards { get; }

        public StaticData()
        {
            Cards = StaticDataParser.ReadDataFromCSV<UnitCard>();
        }
    }
}