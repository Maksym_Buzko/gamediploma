﻿using BattleServer.Models;
using BattleServer.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BattleServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameProcessController : Controller
    {
        private IGameService _gameService;

        public GameProcessController(IGameService gameService)
        {
            _gameService = gameService;
        }

        [HttpGet]
        public IActionResult GetGame(int id)
        {
            return Ok(_gameService.GetGame(id));
        }

        [HttpPost]
        [Route("CreateGame")]
        public IActionResult CreateGame()
        {
            return Ok(_gameService.CreateGame());
        }

        [HttpPost]
        [Route("DeleteGame")]
        public IActionResult DeleteGame(int id)
        {
            _gameService.DeleteGame(id);
            return Ok();
        }

        [HttpPost]
        [Route("SwapCards")]
        public IActionResult SwapCards(int id, int playerId, int card1 = 0, int card2 = 0, int card3 = 0)
        {
            return Ok(_gameService.SwapCards(id, playerId, card1, card2, card3));
        }

        [HttpPost]
        [Route("placecard")]
        public IActionResult PlaceACard(int gameId, int playerId, int cardId)
        {
            return Ok(_gameService.PlaceACard(gameId, playerId, cardId));
        }

        [HttpPost]
        [Route("pass")]
        public IActionResult Pass(int gameId, int playerId)
        {
            return Ok(_gameService.Pass(gameId, playerId));
        }

        [HttpPost]
        [Route("giveup")]
        public IActionResult GiveUp(int gameId, int playerId)
        {
            return Ok(_gameService.GiveUp(gameId, playerId));
        }
    }
}
