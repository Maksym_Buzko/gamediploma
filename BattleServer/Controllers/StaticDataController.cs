﻿using BattleServer.StaticData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BattleServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StaticDataController : Controller
    {
        private IStaticData _staticData;

        public StaticDataController(IStaticData staticData)
        {
            _staticData = staticData;
        }

        [HttpGet]
        public IActionResult getStaticData()
        {
            var staticData = _staticData;
            return Ok(staticData);
        }
    }
}
