﻿using BattleServer.Models;
using BattleServer.StaticData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BattleServer.Services
{
    public interface IGameService
    {
        Game CreateGame();
        public void DeleteGame(int id);
        Game GetGame(int id);
        Game SwapCards(int gameId, int playerId, int card1 = 0, int card2 = 0, int card3 = 0);
        Game Pass(int gameId, int playerId);
        Game PlaceACard(int gameId, int playerId, int cardId);
        Game GiveUp(int gameId, int playerId);
    }

    public class GameService : IGameService
    {
        private BattleDataContext _context;
        private IStaticData _staticData;

        public GameService(
            BattleDataContext context, IStaticData staticData
            )
        {
            _context = context;
            _staticData = staticData;
        }

        public Game GetGame(int id)
        {
            var game = _context.Game.SingleOrDefault(x => x.Id == id);
            return game;
        }

        public Game CreateGame()
        {
            try
            {
                DeleteGame(1);
            }
            catch
            {

            }
            var firstPlayerDeck = new List<int>();
            var secondPlayerDeck = new List<int>();

            for (int i = 0; i < 15; i++)
            {
                firstPlayerDeck.Add(_staticData.Cards[i].Id);
            }
            for (int i = 15; i < 30; i++)
            {
                secondPlayerDeck.Add(_staticData.Cards[i].Id);
            }

            Random random = new Random();
            var firstPlayerHand = new List<int>();
            var secondPlayerHand = new List<int>();

            for (int i = 0; i < 10; i++)
            {
                var a = random.Next(0, firstPlayerDeck.Count);
                firstPlayerHand.Add(firstPlayerDeck[a]);
                firstPlayerDeck.RemoveAt(a);
                secondPlayerHand.Add(secondPlayerDeck[a]);
                secondPlayerDeck.RemoveAt(a);
            }

            var game = new Game()
            {
                Id = 1,
                FirstPlayerId = 1,
                SecondPlayerId = 2,
                FirstPlayerFraction = 1,
                SecondPlayerFraction = 2,
                PLayersTurn = 1,
                FirstPlayerDeck = firstPlayerDeck,
                SecondPlayerDeck = secondPlayerDeck,
                FirstPlayerHand = firstPlayerHand,
                SecondPlayerHand = secondPlayerHand,
                Winner = -1,
                FirstPlayerBoard = new List<int>(),
                SecondPlayerBoard = new List<int>(),
                FirstPlayerUsedCards = new List<int>(),
                SecondPlayerUsedCards = new List<int>(),
            };

            _context.Game.Add(game);
            _context.SaveChanges();
            return game;
        }

        public void DeleteGame(int id)
        {
            var game = _context.Game.SingleOrDefault(x => x.Id == id);
            _context.Game.Remove(game);
            _context.SaveChanges();
        }

        public Game SwapCards(int gameId, int playerId, int card1 = 0, int card2 = 0, int card3 = 0)
        {
            var game = _context.Game.SingleOrDefault(x => x.Id == gameId);

            Random random = new Random();

            if (game.FirstPlayerId == playerId)
            {
                if (card1 != 0)
                {
                    game.FirstPlayerHand.RemoveAt(card1);
                    game.FirstPlayerHand.Add(game.FirstPlayerDeck[0]);
                    game.FirstPlayerDeck.RemoveAt(0);
                }
                if (card2 != 0)
                {
                    game.FirstPlayerHand.RemoveAt(card2);
                    game.FirstPlayerHand.Add(game.FirstPlayerDeck[0]);
                    game.FirstPlayerDeck.RemoveAt(0);
                }
                if (card3 != 0)
                {
                    game.FirstPlayerHand.RemoveAt(card3);
                    game.FirstPlayerHand.Add(game.FirstPlayerDeck[0]);
                    game.FirstPlayerDeck.RemoveAt(0);
                }
            }
            else
            {
                if (card1 != 0)
                {
                    game.SecondPlayerHand.RemoveAt(card1);
                    game.SecondPlayerHand.Add(game.SecondPlayerDeck[0]);
                    game.SecondPlayerDeck.RemoveAt(0);
                }
                if (card2 != 0)
                {
                    game.SecondPlayerHand.RemoveAt(card2);
                    game.SecondPlayerHand.Add(game.SecondPlayerDeck[0]);
                    game.SecondPlayerDeck.RemoveAt(0);
                }
                if (card3 != 0)
                {
                    game.SecondPlayerHand.RemoveAt(card3);
                    game.SecondPlayerHand.Add(game.SecondPlayerDeck[0]);
                    game.SecondPlayerDeck.RemoveAt(0);
                }
            }

            _context.Game.Update(game);
            _context.SaveChanges();
            return game;
        }

        public Game PlaceACard(int gameId, int playerId, int cardId)
        {
            var game = _context.Game.SingleOrDefault(x => x.Id == gameId);

            if (playerId == game.FirstPlayerId)
            {
                game.FirstPlayerBoard.Add(cardId);
                if (!game.SecondPlayerPassed)
                {
                    game.PLayersTurn = game.SecondPlayerId;
                }
                game.FirstPlayerBoardPower = CountPower(game).Item1;
                game.SecondPlayerBoardPower = CountPower(game).Item2;
                game.FirstPlayerHand.Remove(cardId);
                _context.Game.Update(game);
                _context.SaveChanges();
                return game;
            }
            else
            {
                game.SecondPlayerBoard.Add(cardId);
                if (!game.FirstPlayerPassed)
                {
                    game.PLayersTurn = game.FirstPlayerId;
                }
                game.FirstPlayerBoardPower = CountPower(game).Item1;
                game.SecondPlayerBoardPower = CountPower(game).Item2;
                game.SecondPlayerHand.Remove(cardId);
                _context.Game.Update(game);
                _context.SaveChanges();
                return game;
            }
        }

        public Game Pass(int gameId, int playerId)
        {
            var game = _context.Game.SingleOrDefault(x => x.Id == gameId);

            if (playerId == game.FirstPlayerId)
            {
                game.FirstPlayerPassed = true;

                if (!game.SecondPlayerPassed)
                {
                    game.PLayersTurn = game.SecondPlayerId;
                }
            }
            else
            {
                game.SecondPlayerPassed = true;

                if (!game.SecondPlayerPassed)
                {
                    game.PLayersTurn = game.SecondPlayerId;
                }
            }

            _context.Game.Update(game);
            _context.SaveChanges();

            if (game.FirstPlayerPassed && game.SecondPlayerPassed)
            {
                FinishRound(gameId);
            }

            return game;
        }

        public Game GiveUp(int gameId, int playerId)
        {
            var game = _context.Game.SingleOrDefault(x => x.Id == gameId);

            if (game.FirstPlayerId == playerId)
            {
                game.Winner = game.SecondPlayerId;
            }
            if (game.SecondPlayerId == playerId)
            {
                game.Winner = game.FirstPlayerId;
            }

            _context.Game.Update(game);
            _context.SaveChanges();

            return game;
        }

        private Game FinishRound(int gameId)
        {
            var game = _context.Game.SingleOrDefault(x => x.Id == gameId);

            if (game.FirstPlayerBoardPower > game.SecondPlayerBoardPower)
            {
                game.FirstPlayerScore++;
            }
            if (game.FirstPlayerBoardPower < game.SecondPlayerBoardPower)
            {
                game.SecondPlayerScore++;
            }

            if (game.FirstPlayerScore == 2)
            {
                game.Winner = game.FirstPlayerId;
            }
            if (game.SecondPlayerScore == 2)
            {
                game.Winner = game.SecondPlayerId;
            }

            game.FirstPlayerBoard = new List<int>();
            game.SecondPlayerBoard = new List<int>();
            game.FirstPlayerPassed = false;
            game.SecondPlayerPassed = false;
            game.RoundNumber++;

            _context.Game.Update(game);
            _context.SaveChanges();

            return game;
        }

        private (double, double) CountPower(Game game)
        {
            var firstPlayerBoard = _staticData.Cards.Where(value => game.FirstPlayerBoard.Contains(value.Id)).ToList();
            var secondPlayerBoard = _staticData.Cards.Where(value => game.SecondPlayerBoard.Contains(value.Id)).ToList();

            var firstPlayerFootmans = firstPlayerBoard.Where(item => item.Type == GameEnums.UnitType.Footman);
            var firstPlayerArchers = firstPlayerBoard.Where(item => item.Type == GameEnums.UnitType.Marksman);
            var firstPlayerCavalry = firstPlayerBoard.Where(item => item.Type == GameEnums.UnitType.Cavalry);
            var firstPlayerArtillery = firstPlayerBoard.Where(item => item.Type == GameEnums.UnitType.Artillery);
            var secondPlayerFootman = secondPlayerBoard.Where(item => item.Type == GameEnums.UnitType.Footman);
            var secondPlayerArchers = secondPlayerBoard.Where(item => item.Type == GameEnums.UnitType.Marksman);
            var secondPlayerCavalry = secondPlayerBoard.Where(item => item.Type == GameEnums.UnitType.Cavalry);
            var secondPlayerArtillery = secondPlayerBoard.Where(item => item.Type == GameEnums.UnitType.Artillery);

            double firstPlayerFootmanPower = firstPlayerFootmans.Select(item => item.Attack).Sum();
            double firstPlayerArchersPower = firstPlayerArchers.Select(item => item.Attack).Sum();
            double firstPlayerCavalryPower = firstPlayerCavalry.Select(item => item.Attack).Sum();
            double firstPlayerArtilleryPower = firstPlayerArtillery.Select(item => item.Attack).Sum();
            double secondPlayerFootmanPower = secondPlayerFootman.Select(item => item.Attack).Sum();
            double secondPlayerArchersPower = secondPlayerArchers.Select(item => item.Attack).Sum();
            double secondPlayerCavalryPower = secondPlayerCavalry.Select(item => item.Attack).Sum();
            double secondPlayerArtilleryPower = secondPlayerArtillery.Select(item => item.Attack).Sum();

            double firstPlayerFootmanFinalPower = firstPlayerFootmans.Select(item => item.Attack).Sum();
            double firstPlayerArchersFinalPower = firstPlayerArchers.Select(item => item.Attack).Sum();
            double firstPlayerCavalryFinalPower = firstPlayerCavalry.Select(item => item.Attack).Sum();
            double firstPlayerArtilleryFinalPower = firstPlayerArtillery.Select(item => item.Attack).Sum();
            double secondPlayerFootmanFinalPower = secondPlayerFootman.Select(item => item.Attack).Sum();
            double secondPlayerArchersFinalPower = secondPlayerArchers.Select(item => item.Attack).Sum();
            double secondPlayerCavalryFinalPower = secondPlayerCavalry.Select(item => item.Attack).Sum();
            double secondPlayerArtilleryFinalPower = secondPlayerArtillery.Select(item => item.Attack).Sum();

            //first player artillery attacks
            if (firstPlayerArtilleryPower > secondPlayerCavalry.Select(item => item.Defense).Sum() + secondPlayerFootmanPower / 2)
            {
                secondPlayerCavalryFinalPower /= 2;
            }

            //seconf players artillery attacks
            if (secondPlayerArtilleryPower > firstPlayerCavalry.Select(item => item.Defense).Sum() + firstPlayerFootmanPower / 2)
            {
                firstPlayerCavalryFinalPower /= 2;
            }

            //first players cavalry attacks
            if (firstPlayerCavalryPower > secondPlayerArchers.Select(item => item.Defense).Sum() + secondPlayerArtilleryPower / 2)
            {
                secondPlayerArchersFinalPower /= 2;
            }

            //second players cavalry attacks
            if (secondPlayerCavalryPower > firstPlayerArchers.Select(item => item.Defense).Sum() + firstPlayerArtilleryPower / 2)
            {
                firstPlayerArchersFinalPower /= 2;
            }

            //first players archers attack
            if (firstPlayerArchersPower > secondPlayerFootman.Select(item => item.Defense).Sum() + secondPlayerCavalryPower / 2)
            {
                secondPlayerFootmanFinalPower /= 2;
            }

            //second players archers attack
            if (secondPlayerArchersPower > firstPlayerFootmans.Select(item => item.Defense).Sum() + firstPlayerCavalryPower / 2)
            {
                firstPlayerFootmanFinalPower /= 2;
            }

            //first players footmans attack
            if (firstPlayerFootmanPower > secondPlayerArtillery.Select(item => item.Defense).Sum() + secondPlayerArchersPower / 2)
            {
                secondPlayerArtilleryFinalPower /= 2;
            }

            //second players footmans attack
            if (secondPlayerFootmanPower > firstPlayerArtillery.Select(item => item.Defense).Sum() + firstPlayerArchersPower / 2)
            {
                firstPlayerArtilleryFinalPower /= 2;
            }

            return (firstPlayerArtilleryFinalPower + firstPlayerArchersFinalPower + firstPlayerCavalryFinalPower + firstPlayerFootmanFinalPower,
                    secondPlayerArtilleryFinalPower + secondPlayerArchersFinalPower + secondPlayerCavalryFinalPower + secondPlayerFootmanFinalPower);
        }
    }
}
