﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BattleServer.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Game",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    FirstPlayerId = table.Column<int>(nullable: false),
                    SecondPlayerId = table.Column<int>(nullable: false),
                    FirstPlayerFraction = table.Column<int>(nullable: false),
                    SecondPlayerFraction = table.Column<int>(nullable: false),
                    PLayersTurn = table.Column<int>(nullable: false),
                    FirstPlayerBoard = table.Column<string>(nullable: true),
                    FirstPlayerBoardPower = table.Column<double>(nullable: false),
                    FirstPlayerPassed = table.Column<bool>(nullable: false),
                    SecondPlayerBoard = table.Column<string>(nullable: true),
                    SecondPlayerBoardPower = table.Column<double>(nullable: false),
                    SecondPlayerPassed = table.Column<bool>(nullable: false),
                    FirstPlayerDeck = table.Column<string>(nullable: true),
                    FirstPlayerHand = table.Column<string>(nullable: true),
                    FirstPlayerUsedCards = table.Column<string>(nullable: true),
                    SecondPlayerDeck = table.Column<string>(nullable: true),
                    SecondPlayerHand = table.Column<string>(nullable: true),
                    SecondPlayerUsedCards = table.Column<string>(nullable: true),
                    RoundNumber = table.Column<int>(nullable: false),
                    FirstPlayerScore = table.Column<int>(nullable: false),
                    SecondPlayerScore = table.Column<int>(nullable: false),
                    Winner = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Game", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Game");
        }
    }
}
