﻿namespace BattleServer.GameEnums
{
    public enum RarityType
    {
        Common,
        Rare,
        Epic,
        Legendary
    }
}