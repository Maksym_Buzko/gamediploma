﻿namespace BattleServer.GameEnums
{
    public enum UnitType
    {
        Footman,
        Marksman,
        Cavalry,
        Artillery
    }
}