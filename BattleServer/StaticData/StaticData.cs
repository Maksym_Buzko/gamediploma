﻿using BattleServer.GameModels;
using System.Collections.Generic;

namespace BattleServer.StaticData
{
    public interface IStaticData
    {
        public List<UnitCard> Cards { get; }
    }

    public class StaticData : IStaticData
    {
        public List<UnitCard> Cards { get; }

        public StaticData()
        {
            Cards = StaticDataParser.ReadDataFromCSV<UnitCard>();
        }
    }
}
